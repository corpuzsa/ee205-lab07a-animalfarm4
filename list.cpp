///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
///
/// @author Shaun Corpuz <corpuzsa@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   03/23/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cstdlib>

#include "list.hpp"

namespace animalfarm{

   Node* head = new Node();
   
   const bool SingleLinkedList::empty() const{

      return head == nullptr;
   }

   void SingleLinkedList::push_front( Node* newNode ){
      newNode->next = head;
      head = newNode;
   }

   Node* SingleLinkedList::pop_front(){
      Node* tmp = head;
      head = head->next;
      return tmp;
   }

   Node* SingleLinkedList::get_first() const{
      return head;
   }

   Node* SingleLinkedList::get_next(const Node* currentNode) const{
      return currentNode->next;
   }

   unsigned int SingleLinkedList::size() const{
      unsigned int size;

      Node* tmp = head;

      for(size = 0;tmp!=nullptr; size++){
         size=size + 1;
         tmp=tmp->next;
      }
      return size;
   }
}

