///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Shaun Corpuz <corpuzsa@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02/27/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

#include "animalfactory.hpp"
#include "node.hpp"
#include "list.hpp"

#include <array>
#include <list>

using namespace std;
using namespace animalfarm;

int main(){
  
   // Making the array containers
   cout << "Welcome to Animal Farm 4" << endl;

   SingleLinkedList animalList;

   for(auto i = 0;i < 25; i++){
      animalList.push_front( AnimalFactory::getRandomAnimal() );
   }

   cout << endl;
   cout << "List of Animals" << endl;
   cout << "   Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << "   Number of elements: " << animalList.size() << endl;

   for(auto animal = animalList.get_first(); animal != nullptr; animal = animalList.get_next(animal)){
      cout << ((Animal*) animal)->speak() << endl;
   }

   while(!animalList.empty()){
      Animal* animal = (Animal*) animalList.pop_front();
      delete animal;
   }
   cout << endl;
}

