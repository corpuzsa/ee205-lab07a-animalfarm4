///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.hpp
/// @version 1.0
///
///
/// @author Shaun Corpuz <corpuzsa@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   03/23/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
using namespace std;

namespace animalfarm{

class Node {
   protected:
      Node* next = nullptr;

   friend class SingleLinkedList;
};

}
